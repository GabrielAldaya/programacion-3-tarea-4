// programacion 3 tarea 4.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include "Poco\Net\SocketAddress.h"
#include "Poco\Net\StreamSocket.h"
#include "Poco\Net\SocketStream.h"
#include "Poco\StreamCopier.h"
#include "Poco\JSON\Parser.h"
#include "Poco\JSON\ParseHandler.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/StreamCopier.h"
#include "Poco\URI.h"
#include <Poco/Dynamic/Var.h>

using namespace std;
using namespace Poco;
using namespace Poco::Net;
using namespace Poco::JSON;

string url;

bool doRequest(Poco::Net::HTTPClientSession& session, Poco::Net::HTTPRequest& request, Poco::Net::HTTPResponse& response)
{
	session.sendRequest(request);
	std::istream& rs = session.receiveResponse(response);
	std::cout << response.getStatus() << " " << response.getReason() << std::endl;
	if (response.getStatus() != Poco::Net::HTTPResponse::HTTP_UNAUTHORIZED)
	{
		std::ofstream ofs(url, std::fstream::binary);
		StreamCopier::copyStream(rs, ofs);
		return true;
	}
	else
	{
		//it went wrong ?
		return false;
	}
}

int main()
{
	url = "http://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22dallas%2C%20tx%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

	URI uri(url);
	std::string path(uri.getPathAndQuery());
	if (path.empty()) path = "/";

	HTTPClientSession session(uri.getHost(), uri.getPort());
	HTTPRequest request(HTTPRequest::HTTP_GET, path, HTTPMessage::HTTP_1_1);
	HTTPResponse response;

	if (!doRequest(session, request, response))
	{
		std::cerr << "Invalid username or password" << std::endl;
		return 1;
	}
/*
	string coso ="{\"query\":{\"count\":1, \"created\" : \"2017-06-06T05:01:25Z\", \"lang\" : \"es-ES\", \"results\" : {\"channel\":{\"item\":{\"condition\":{\"text: \"Mostly Cloudy\"\"}}}}}}";

	//std::string json = "{ \"name\" : \"Franky\", \"children\" : [ \"Jonas\", \"Ellen\" ] }";
	std::string json = coso;
	Parser parser;
	Poco::Dynamic::Var result = parser.parse(json);
	//std::cout << result << endl;
	Object::Ptr object = result.extract<Object::Ptr>();
	Poco::Dynamic::Var test = object->get("text");
	object = test.extract<Object::Ptr>();
	test = object->get("text");
	std::string value = test.convert<std::string>();
	*/
	std::cin.get();
    return 0;
}

